package feula.net.roll20

const val PREFS_ROLL20_USERNAME = "roll20_username"
const val PREFS_ROLL20_PASSWORD = "roll20_password"