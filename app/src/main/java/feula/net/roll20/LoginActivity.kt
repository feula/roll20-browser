package feula.net.roll20

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModel
import android.content.Intent
import android.databinding.Bindable
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import androidx.core.content.edit
import feula.net.roll20.auth.HTMLAuthProvider
import feula.net.roll20.databinding.ActivityLoginBinding
import feula.net.roll20.extensions.*
import feula.net.roll20.extensions.android.livedata.SingleLiveEvent
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.launch

const val FLAG_USER_NOT_AUTHED = "USER_NOT_AUTHED"

/**
 * Activity responsible for authenticating the user.
 * Displays a simple login screen that takes Roll20 credentials, and ensures that they work.
 */
class LoginActivity : AppCompatActivity() {
    class VM(app: Application) : AndroidViewModel(app) {
        private val _username = MutableLiveData<String>()
        private val _password = MutableLiveData<String>()
        private val authProvider = HTMLAuthProvider()

        val requestsSnackbarForLoginError = SingleLiveEvent<Unit>()
        val requestsSnackbarForNetworkError = SingleLiveEvent<Unit>()
        val requestsSavingUserInfo        = SingleLiveEvent<Unit>()

        var username: String
            get() = _username.value ?: ""
            set(value) { _username.value = value }

        var password: String
            get() = _password.value ?: ""
            set(value) { _password.value = value }

        init {
            authProvider.failedWithNetworkError.observeForever { requestsSnackbarForNetworkError.call() }
            authProvider.failedWithWrongCredentials.observeForever {
                requestsSnackbarForLoginError.call()
            }
        }

        private fun verifyCredentials(username: String, password: String) {
            authProvider.authenticate(username, password).observeForever {
                // We may receive different kinds of errors here, most notably
                // all the network error exceptions
                when (it) {
                    is IllegalStateException -> requestsSnackbarForLoginError.call()
                    is Exception -> requestsSnackbarForNetworkError.call()
                    is Nothing? -> requestsSavingUserInfo.call()
                }
            }
        }
         fun checkAuthInfo() {
            async { verifyCredentials(username, password) }
        }
    }

    private val vm: LoginActivity.VM           by lazyViewModelOf(this)
    private val bindings: ActivityLoginBinding by lazyActivityBindingOf(this, R.layout.activity_login)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        bindings.vm = vm
        bindings.setLifecycleOwner(this)

        vm.requestsSnackbarForNetworkError.observe(this, Observer {
            Snackbar.make(
                    findViewById(R.id.login_root_layout),
                    getString(R.string.login_login_error_message_network),
                    Snackbar.LENGTH_LONG
            ).show()
        })
        vm.requestsSnackbarForLoginError.observe(this, Observer {
            Snackbar.make(
                    findViewById(R.id.login_root_layout),
                    getString(R.string.login_login_error_message_credentials),
                    Snackbar.LENGTH_LONG
            ).show()
        })
        vm.requestsSavingUserInfo.observe(this, Observer {
            val prefs = PreferenceManager.getDefaultSharedPreferences(this)
            prefs.edit {
                putString(PREFS_ROLL20_PASSWORD, vm.password)
                putString(PREFS_ROLL20_USERNAME, vm.username)
            }
            startActivity(Intent(this, MainActivity::class.java))
        })

        /*username_input.onTextChange(after = {
            vm.username = it?.toString() ?: ""
        })
        password_input.onTextChange(after = {
            vm.password = it?.toString() ?: ""
        })*/
    }

    override fun onStart() {
        super.onStart()
        if (intent.getBooleanExtra(FLAG_USER_NOT_AUTHED, false)) {
            Toast.makeText(this, "You are not authenticated", Toast.LENGTH_LONG).show()
        }

    }

    override fun onBackPressed() {
        startActivity(Intent(Intent.ACTION_MAIN).apply { addCategory(Intent.CATEGORY_HOME) })
    }


}