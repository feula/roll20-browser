package feula.net.roll20

import android.app.Application
import android.arch.lifecycle.*
import android.content.Intent
import android.databinding.DataBindingUtil
import android.opengl.Visibility
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import android.support.v7.util.DiffUtil
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import feula.net.roll20.auth.HTMLAuthProvider
import feula.net.roll20.campaign.*
import feula.net.roll20.databinding.ActivityMainBinding
import feula.net.roll20.extensions.android.livedata.SingleLiveEvent
import feula.net.roll20.extensions.lazyActivityBindingOf
import feula.net.roll20.extensions.lazyViewModelOf
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    class VM : ViewModel() {
        val campaigns: LiveData<List<Campaign>>
        val hasCampaigns: LiveData<Boolean>

        private val authProvider = HTMLAuthProvider()
        private val wrongCredentialsListener: LiveData<Unit>

        val userIsNotAuthenticated =  SingleLiveEvent<Unit>()

        init {
            campaigns = Transformations.switchMap(authProvider.client, { it?.let {
                Log.d("TAG", "Changing campaigns!")
                CampaignRepo(HTMLCampaignProvider(it)).loadCampaigns()
            }})
            hasCampaigns = Transformations.map(campaigns, {
                Log.d("TAG", "Campaign list now has ${it.size} elements")
                it.isNotEmpty()
            })
            wrongCredentialsListener = Transformations.switchMap(authProvider.failedWithWrongCredentials, {
                Log.d("TAG", "registering mediator")
                val mediator = MediatorLiveData<Unit>()
                mediator.addSource(authProvider.failedWithWrongCredentials, {
                    Log.d("TAG", "calling listener")
                    userIsNotAuthenticated.call()
                })
                mediator
            })
        }

        fun authenticate(username: String, password: String) {
            authProvider.authenticate(username, password)
        }
    }

    private val vm: MainActivity.VM by lazyViewModelOf(this)
    private val bindings: ActivityMainBinding by lazyActivityBindingOf(this, R.layout.activity_main)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindings.vm = vm
        bindings.setLifecycleOwner(this)

        campaign_list.let {
            it.layoutManager = LinearLayoutManager(this)
            campaign_list.adapter = CampaignCardListAdapter(mutableListOf())
        }

        navigation.setOnNavigationItemSelectedListener { true }

        vm.campaigns.observe(this, Observer {
            (campaign_list.adapter as CampaignCardListAdapter).updateListWith(it?.toMutableList() ?: mutableListOf())
        })

        vm.userIsNotAuthenticated.observe(this, Observer {
            Log.d("TAG", "Dude we're supposed to")
            // We didn't manage to login. Throw the user back to login
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        })

        vm.hasCampaigns.observe(this, Observer {
            Log.d("TAG", "Do we have campaigns now?: $it")
            campaign_list_empty.visibility = if (it!!) View.GONE else View.VISIBLE
            campaign_list.visibility = if (it) View.VISIBLE else View.GONE
        })

        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        prefs.edit().apply {
            putString(PREFS_ROLL20_PASSWORD, "")
            putString(PREFS_ROLL20_USERNAME, "")
            apply()
        }

        val username = prefs.getString(PREFS_ROLL20_USERNAME, "")
        val password = prefs.getString(PREFS_ROLL20_PASSWORD, "")
        vm.authenticate(username, password)
    }
}
