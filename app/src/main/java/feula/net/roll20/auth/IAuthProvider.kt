package feula.net.roll20.auth

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.util.Log
import feula.net.roll20.extensions.android.livedata.SingleLiveEvent
import kotlinx.coroutines.experimental.Deferred
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.defer
import okhttp3.*
import org.joda.time.Chronology
import org.joda.time.DateTime
import org.jsoup.Jsoup

interface IAuthProvider {
    val client: MutableLiveData<OkHttpClient?>
    fun authenticate(username: String, password: String): LiveData<OkHttpClient?>
    fun disconnect()
}

/**
 * Provides authenticated access to roll20. Every request made with the
 * client given back by [authenticate] should succeed, whether sending or
 * receiving.
 */
class HTMLAuthProvider(private val http: OkHttpClient = OkHttpClient()) : IAuthProvider {
    override val client = MutableLiveData<OkHttpClient?>()

    val failedWithWrongCredentials = SingleLiveEvent<Unit>()
    val failedWithNetworkError     = SingleLiveEvent<Unit>()

    private val TAG = HTMLAuthProvider::class.java.name
    private val authRequest = Request.Builder()
            .url("https://app.roll20.net/sessions/create")
    private val destroyRequest = Request.Builder()
            .url("https://app.roll20.net/sessions/destroy")
    private var authCookie: String? = null
    private var authExpiresAt: DateTime? = null
    /**
     * Intercepts and sets cookies for us. This means that every request done with
     * this jar will allow us to authenticate immediately (and get a cookie if it is
     * changed or anything)
     */
    private val cookieInterceptor: CookieJar = object : CookieJar {
        override fun saveFromResponse(url: HttpUrl?, cookies: MutableList<Cookie>?) {
            cookies?.firstOrNull { it.name() == "rack.session" }?.let {
                authCookie = it.value()
                authExpiresAt = DateTime(it.expiresAt())
            }
        }

        override fun loadForRequest(url: HttpUrl?): MutableList<Cookie> {
            return authCookie?.let {
                mutableListOf(Cookie.Builder()
                        .domain("roll20.net")
                        .name("rack.session")
                        .secure()
                        .httpOnly()
                        .path("/")
                        .value(it)
                        .build())
            } ?: mutableListOf()

        }
    }

    private fun ensureCookieIsSet() {
        if (authCookie.isNullOrBlank()) {
            throw IllegalStateException("Could not get a working cookie for queries. Are you sure you called authenticate?")
        }
        Log.d(TAG, "Cookie seems good.")
    }

    private fun ensureResponseSeemsToMatch(res: Response) {
        val parsed = res.body()!!.string()
        val matches = when {
            parsed.contains("The email or password you entered is incorrect") -> false
            parsed.contains("Create an Account") -> false
            parsed.contains("Hours Played") -> true
            parsed.contains("Member since") -> true
            else -> false
        }

        if (!matches) {
            throw IllegalStateException("Couldn't find anything indicating that we're logged in")
        }

        Log.d(TAG, "Response seems good.")

    }

    /**
     * Authenticates with Roll20 and send back a OKHttpClient that is able to make authenticated requests
     */
    override fun authenticate(username: String, password: String): LiveData<OkHttpClient?> {
        Log.d(TAG, "Authenticating the user by POSTing directly to the page")
        Log.d(TAG, "Username: $username, Password: $password")
        val client = http.newBuilder()
                .cookieJar(cookieInterceptor)
                .build()

        async {
            val res = client.newCall(authRequest
                    .post(FormBody.Builder()
                            .add("email", username)
                            .add("password", password)
                            .build())
                    .build())
                    .execute()

            try {
                ensureCookieIsSet()
                ensureResponseSeemsToMatch(res)
                this@HTMLAuthProvider.client.postValue(client)
            } catch (e: IllegalStateException) {
                Log.d("Oh shit son", "really hard")
                failedWithWrongCredentials.call()
            }
        }

        return this.client
    }
    /**
     * Sends a request to Roll20 to destroy the session, effectively disconnecting you.
     * Your previous HTTP client may be reused as the cookie will now be ignored, but
     * this method returns a new one in you need it for any reason.
     */
    override fun disconnect() {
        async {
            val client = http.newBuilder().build()
            client.newCall(destroyRequest.get().build()).execute()
            this@HTMLAuthProvider.client.postValue(null)
        }
    }
}