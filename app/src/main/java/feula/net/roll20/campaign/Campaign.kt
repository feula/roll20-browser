package feula.net.roll20.campaign

import android.databinding.BaseObservable
import android.databinding.Bindable
import android.support.v7.util.DiffUtil

data class Campaign(
        val title: String,
        val url: String,
        val imageUrl: String) {

    data class ViewModel(private val campaign: Campaign) : BaseObservable() {
        @Bindable fun getTitle() = campaign.title
        @Bindable fun getUrl() = campaign.url
        fun getImageUrl() = campaign.imageUrl
    }

}

fun Campaign.toViewModel() = Campaign.ViewModel(this)

class CampaignDifferCallback(
        private val old: List<Campaign>,
        private val new: List<Campaign>): DiffUtil.Callback() {
    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
            old[oldItemPosition].url == new[newItemPosition].url

    override fun getOldListSize() =
            old.size

    override fun getNewListSize() =
            new.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) =
            old[oldItemPosition] == new[newItemPosition]

}