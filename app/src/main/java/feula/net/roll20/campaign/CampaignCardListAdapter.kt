package feula.net.roll20.campaign

import android.animation.Animator
import android.animation.AnimatorInflater
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.app.Activity
import android.content.Context
import android.databinding.BindingAdapter
import android.databinding.DataBindingUtil
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import com.squareup.picasso.Picasso
import feula.net.roll20.R
import feula.net.roll20.databinding.CampaignCardBinding


class CampaignCardListAdapter(private val dataset: MutableList<Campaign>)
    : RecyclerView.Adapter<CampaignCardListAdapter.ViewHolder>() {

    private lateinit var ctx: Context

    private fun getEnterAnimation(view: View) = AnimatorSet().apply {
        playTogether(
                AnimatorInflater.loadAnimator(ctx, R.animator.item_fade_in),
                AnimatorInflater.loadAnimator(ctx, R.animator.item_slide_in_from_left)
        )
        setTarget(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        this.ctx = parent.context
        return ViewHolder(
                DataBindingUtil.inflate(
                        LayoutInflater.from(parent.context),
                        R.layout.campaign_card,
                        parent, false
                ))
    }

    override fun getItemCount(): Int = dataset.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val vm = dataset[position].toViewModel()
        holder.itemView.animation = AnimationUtils.loadAnimation(ctx, R.anim.item_slide_and_fade)
        holder.itemView.animate()
        holder.bind(vm)
        Picasso.get().load(vm.getImageUrl())
                .fit()
                .centerCrop()
                .error(R.drawable.ic_cloud_off_black_24dp)
                .into(holder.binding.imageHeader)
    }

    fun updateListWith(campaigns: List<Campaign>) {
        val result = DiffUtil.calculateDiff(CampaignDifferCallback(dataset, campaigns))
        this.dataset.clear()
        this.dataset.addAll(campaigns)
        result.dispatchUpdatesTo(this)
    }

    class ViewHolder(val binding: CampaignCardBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(vh: Campaign.ViewModel) {
            binding.data = vh
            binding.executePendingBindings()
        }
    }
}