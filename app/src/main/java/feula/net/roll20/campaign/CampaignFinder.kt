package feula.net.roll20.campaign

import org.jsoup.Jsoup
import org.jsoup.nodes.Document

interface ICampaignFinder {

}

class CampaignFinder : ICampaignFinder {
    fun campaigns(page: Document): List<Campaign> {
        val domCampaigns = page.select(".homegamelist .listing")

        return domCampaigns.map {
            Campaign(
                    title = it.select(".gameinfo a:nth-child(1)").text(),
                    imageUrl = it.select(".imgcontainer a:nth-child(1) img").attr("src"),
                    url = it.select(".gameinfo a")[1].attr("href")
            )
        }
    }
}