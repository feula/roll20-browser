package feula.net.roll20.campaign

import okhttp3.OkHttpClient
import okhttp3.Request
import org.jsoup.Jsoup

/**
 * Retrieves campaigns from Roll20 using [http] to make requests.
 * It is advised to use a client that is actually authenticated.
 */
class HTMLCampaignProvider(val http: OkHttpClient) {
    private val campaignListRequest = Request.Builder()
            .url("https://roll20.net/")

    fun getCampaigns(): List<Campaign> {
        val response = http.newCall(campaignListRequest.get().build()).execute()
        val responseAsString = response.body()?.string()

        val parsed = Jsoup.parse(responseAsString)
        return CampaignFinder().campaigns(parsed)
    }
}