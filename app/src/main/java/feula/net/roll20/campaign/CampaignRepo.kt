package feula.net.roll20.campaign

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import kotlinx.coroutines.experimental.async

class CampaignRepo(private val campaignProvider: HTMLCampaignProvider) {
    val campaigns = MutableLiveData<List<Campaign>>()

    fun loadCampaigns(): LiveData<List<Campaign>> {
        async { campaigns.postValue(campaignProvider.getCampaigns()) }

        return campaigns
    }
}