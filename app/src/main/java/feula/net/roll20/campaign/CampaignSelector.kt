package feula.net.roll20.campaign

import okhttp3.OkHttpClient
import okhttp3.Request

class CampaignSelector(private val http: OkHttpClient) {
    /*
     * Apparently, telling Roll20 which campaign to use is simply a matter of GETting
     * its url, then the editor will always return that info.
     */
    fun select(url: String) {
        http.newCall(
                Request.Builder()
                        .url(url)
                        .get()
                        .build()
        ).execute()
    }
}