package feula.net.roll20.character

import android.util.Log
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.jsoup.Jsoup
import org.jsoup.nodes.Document

class CharacterProvider(private val http: OkHttpClient) {
    val popoutEditorUrl = "https://app.roll20.net/editor/popout"

    fun retrievePage() = http.newCall(Request.Builder().url(popoutEditorUrl).get().build()).execute()
    fun parsePageIntoDocument(response: Response) = Jsoup.parse(response.body()?.string())

    fun parseDocumentForCharacter(document: Document) {
        Log.d(javaClass.name, document.toString())
    }
}