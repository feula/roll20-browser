package feula.net.roll20.extensions

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.app.AppCompatActivity

/**
 * Returns a lazy instance of a [ViewDataBinding] bound to [activity], inflating the layout [layout] in it.
 * Usage:
 *     {@code val bindings: MainActivityBinding by activityBindingOf(this, R.layout.activity_main)}
 *     {@code val bindings by activityBindingOf<MainActivityBinding>(this, R.layout.activity_main)}
 * @param <T> Type of the [ViewDataBinding] instance
 * @param activity: Activity in which the layout should be inflated and the data bound
 * @param layout: Layout to inflate
 * @returns A lazy instance of the bindings
 */
inline fun <reified T : ViewDataBinding> lazyActivityBindingOf(activity: AppCompatActivity, layout: Int): Lazy<T> {
    return lazy { DataBindingUtil.setContentView<T>(activity, layout) }
}