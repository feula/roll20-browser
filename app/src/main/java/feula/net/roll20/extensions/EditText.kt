package feula.net.roll20.extensions

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText

fun EditText.onTextChange(
        after: (Editable?) -> Unit = { },
        before: (CharSequence?, Int, Int, Int) -> Unit = { _, _, _, _ -> } ,
        on: (CharSequence?, Int, Int, Int) -> Unit = { _, _, _, _ -> }) = this.addTextChangedListener(object: TextWatcher {
    override fun afterTextChanged(editable: Editable?) = after(editable)
    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = before(s, start, count, after)
    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) = on(s, start, before, count)
})