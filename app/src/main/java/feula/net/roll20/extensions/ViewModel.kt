package feula.net.roll20.extensions

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity

/**
 * Returns a lazy viewmodel of type [T] for the activity [activity]
 * This method is safe to be called directly at the root of your activity (since it is lazy)
 * Usage:
 *     val vm: MainActivity.VM by viewModelOf(this)
 *     val vm by viewModelOf<MainActivity.VM>(this)
 * @param <T> Type of the ViewModel.
 * @param activity: Activity that should be used to provide the ViewModel (and keep state through recreations)
 * @returns A lazy instance of the specified ViewModel
 */
inline fun <reified T : ViewModel> lazyViewModelOf(activity: AppCompatActivity): Lazy<T> {
    return lazy { ViewModelProviders.of(activity).get(T::class.java) }
}